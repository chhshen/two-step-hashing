


% Code author: Guosheng Lin. Contact: guosheng.lin@gmail.com or guosheng.lin@adelaide.edu.au
% Correspondence should be addressed to Chunhua Shen (chhshen@gmail.com)

% Please cite this paper: 
% "A general two-step approach to learning-based hashing";
% Guosheng Lin, Chunhua Shen, David Suter, and Anton van den Hengel.



addpath(genpath([pwd '/']));



% try larger bits: 32, 64, ...
bit_num=16;


% method running flags:

% run_TSH=false;
% run_LSH=false;

run_TSH=true;
run_LSH=true;


%--------------------------------------------------------------------------------------------------
% load data



% try these datasets:

% ds_file='./datasets/uci_vowel.mat';
% ds_name='VOWEL';

ds_file='./datasets/uci_usps.mat';
ds_name='USPS';

% ds_file='./datasets/pendigits.mat';
% ds_name='PENDIGITS';







% load the demo dataset:
ds=load(ds_file);
ds=ds.dataset;


% here is a very simple way to split training data, just for demo

e_num=length(ds.y);
tst_e_num=min(2000, round(e_num*0.3));
tst_sel=false(e_num,1);
tst_sel(randsample(e_num,tst_e_num))=true;
ds.test_inds=find(tst_sel);

database_sel=~tst_sel;
ds.db_inds=find(database_sel);

db_e_num=length(ds.db_inds);
trn_e_num=min(2000, db_e_num);
ds.train_inds=ds.db_inds(randsample(db_e_num,trn_e_num));


database_data=[];
database_data.feat_data=ds.x(ds.db_inds,:);
database_data.label_data=ds.y(ds.db_inds);

train_data=[];
train_data.feat_data=ds.x(ds.train_inds,:);
train_data.label_data=ds.y(ds.train_inds);

test_data=[];
test_data.feat_data=ds.x(ds.test_inds,:);
test_data.label_data=ds.y(ds.test_inds);



% do normalization.

max_dims=max(train_data.feat_data,[],1);
min_dims=min(train_data.feat_data,[],1);
range_dims=max_dims-min_dims+eps;


database_data.feat_data = bsxfun(@minus, database_data.feat_data, min_dims);
database_data.feat_data = bsxfun(@rdivide, database_data.feat_data, range_dims);

train_data.feat_data = bsxfun(@minus, train_data.feat_data, min_dims);
train_data.feat_data = bsxfun(@rdivide, train_data.feat_data, range_dims);

test_data.feat_data = bsxfun(@minus, test_data.feat_data, min_dims);
test_data.feat_data = bsxfun(@rdivide, test_data.feat_data, range_dims);






% generate pairwise affinity ground truth for supervised hashing learning:
% a simple example is shown here for dataset with multi-class labels.
% users can replace this ground truth definition here according to your applications.

affinity_labels=gen_affinity_labels(train_data);
trn_e_num=size(train_data.feat_data, 1);
assert(size(affinity_labels, 1)==trn_e_num);
assert(size(affinity_labels, 2)==trn_e_num);
train_data.relation_info=gen_relation_info(affinity_labels);
  



%--------------------------------------------------------------------------------------------------


        
    
    
    
if run_TSH
    
    
    tsh_train_info=[];
	tsh_train_info.bit_num=bit_num;
    
    tsh_train_info.hash_loss_type='KSH';
%     tsh_train_info.hash_loss_type='BRE';
%     tsh_train_info.hash_loss_type='Hinge';
    

	% classifier setting for Step-2, users can use any customized classifier, 
	% some examples are included here:

      
      classifier_type='svm_rbf'; 
%     	classifier_type='svm_rbf_budgeted';
      % classifier_type='svm_rbf_kernel_feat';
%       classifier_type='svm_linear';


	hash_learner_param=[];
    hash_learner_param.bit_num=bit_num;
	hash_learner_param.classifier_type=classifier_type;


	tradeoff_param=1e7;
    hash_learner_param.tradeoff_param=tradeoff_param;


	if strcmp(classifier_type, 'svm_rbf') || strcmp(classifier_type, 'svm_rbf_kernel_feat') ...
		|| strcmp(classifier_type, 'svm_rbf_budgeted') 
    
	    % this is a simple example for picking the RBF kernel parameter sigma
	    % user should tune this parameter, e.g., try to pick v from [0.1 1 5 10].

	    trn_feat_data=train_data.feat_data;
	    rbf_knn=50;
	    if size(trn_feat_data,1)>1e4
	        trn_feat_data=trn_feat_data(randsample(size(trn_feat_data,1), 1e4),:);
	    end
	    sq_eudist = sqdist(trn_feat_data',trn_feat_data');
	    sq_eudist=sort(sq_eudist,2);
	    sq_eudist=sq_eudist(:,1:rbf_knn);
	    sq_sigma = mean(sq_eudist(:));
	    sq_sigma=sq_sigma+eps;

	    % this need to tune, try to pick v from [0.1 0.5 1 5 10]
	    v=0.5;
	    hash_learner_param.sigma=sq_sigma*v;

	end


	if strcmp(classifier_type, 'svm_rbf_kernel_feat') 

	    % random select support vectors, as an alternative, user can use
	    % k-means for setting this support_vectors.
	    sv_num=500;
	    sv_num=min(sv_num, size(trn_feat_data, 1));
	    support_vectors=trn_feat_data(randsample(size(trn_feat_data, 1), sv_num),:);
	    hash_learner_param.support_vectors=support_vectors;
    end


    
    if strcmp(classifier_type, 'svm_rbf_budgeted')
        
        addpath([pwd '/libs/BudgetedSVM/matlab/']);
        
        hash_learner_param.epoch_num=10;
        
        % the budget of support vectors:
        hash_learner_param.budget=100;
    end


	
	tsh_train_info.hash_learner_param=hash_learner_param;

    tsh_train_result=tsh_train(tsh_train_info, train_data);
    tsh_model=tsh_train_result.model;

    
    tsh_db_data_code=tsh_encode(tsh_model, database_data.feat_data);
    tsh_tst_data_code=tsh_encode(tsh_model, test_data.feat_data);


    % generate compact code if it is needed:
    % tsh_db_data_code_compact=gen_compactbit(tsh_db_data_code);
    

end





%--------------------------------------------------------------------------------------------------
% run LSH for a simple comparison:


if run_LSH

    lsh_model=LSH_learn(train_data.feat_data, bit_num);
    lsh_db_data_code=LSH_compress(database_data.feat_data, lsh_model);
    lsh_tst_data_code=LSH_compress(test_data.feat_data, lsh_model);

end
















%--------------------------------------------------------------------------------------------------
% evaluate methods:

label_type='multiclass';

db_label_info.label_data=database_data.label_data;
db_label_info.label_type=label_type;

test_label_info.label_data=test_data.label_data;
test_label_info.label_type=label_type;

code_data_info=[];
code_data_info.db_label_info=db_label_info;
code_data_info.test_label_info=test_label_info;




%-----------------------------------------------


eva_bit_step=round(min(8, bit_num/4));
eva_bits=eva_bit_step:eva_bit_step:bit_num;


eva_param=[];
eva_param.eva_top_knn_pk=100;

predict_results=cell(0);











%-----------------------------------------------


if run_TSH
    
    tsh_predict_result=[];
    tsh_predict_result.method_name='TSH';
    tsh_predict_result.eva_bits=eva_bits;
    for b_idx=1:length(eva_bits)
        one_bit_num=eva_bits(b_idx);
        tsh_code_data_info=code_data_info;
        tsh_code_data_info.db_data_code=tsh_db_data_code(:, 1:one_bit_num);
        tsh_code_data_info.tst_data_code=tsh_tst_data_code(:, 1:one_bit_num);
        one_bit_result=hash_evaluate(eva_param, tsh_code_data_info);
        tsh_predict_result.pk100_eva_bits(b_idx)=one_bit_result.pk100;
    end
    predict_results{end+1}=tsh_predict_result;

end




%-----------------------------------------------

if run_LSH

    lsh_predict_result=[];
    lsh_predict_result.method_name='LSH';
    lsh_predict_result.eva_bits=eva_bits;
    for b_idx=1:length(eva_bits)
        one_bit_num=eva_bits(b_idx);
        lsh_code_data_info=code_data_info;
        lsh_code_data_info.db_data_code=lsh_db_data_code(:, 1:one_bit_num);
        lsh_code_data_info.tst_data_code=lsh_tst_data_code(:, 1:one_bit_num);
        one_bit_result=hash_evaluate(eva_param, lsh_code_data_info);
        lsh_predict_result.pk100_eva_bits(b_idx)=one_bit_result.pk100;
    end
    predict_results{end+1}=lsh_predict_result;

end










%-----------------------------------------------


f1=figure;
line_width=2;
xy_font_size=22;
marker_size=10;
legend_font_size=15;
xy_v_font_size=15;
title_font_size=xy_font_size;

legend_strs=cell(length(predict_results), 1);

for p_idx=1:length(predict_results)
    
    predict_result=predict_results{p_idx};
   
    fprintf('\n\n-------------predict_result--------------------------------------------------------\n\n');
    disp(predict_result);

    color=gen_color(p_idx);
    marker=gen_marker(p_idx);
    
    x_values=eva_bits;
    y_values=predict_result.pk100_eva_bits;

    p=plot(x_values, y_values);
    
    set(p,'Color', color)
    set(p,'Marker',marker);
    set(p,'LineWidth',line_width);
    set(p,'MarkerSize',marker_size);
    
    legend_strs{p_idx}=[predict_result.method_name  sprintf('(%.3f)', y_values(end))];
    
    hold all
end


hleg=legend(legend_strs);
h1=xlabel('Number of bits');
h2=ylabel('Precision @ K (K=100)');
title(ds_name, 'FontSize', title_font_size);

set(gca,'XTick',x_values);
xlim([x_values(1) x_values(end)]);
set(hleg, 'FontSize',legend_font_size);
set(hleg,'Location','SouthEast');
set(h1, 'FontSize',xy_font_size);
set(h2, 'FontSize',xy_font_size);
set(gca, 'FontSize',xy_v_font_size);
set(hleg, 'FontSize',legend_font_size);
grid on;
hold off

























