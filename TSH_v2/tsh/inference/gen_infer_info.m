

function infer_info=gen_infer_info(train_info, work_info_step1)
    
    infer_info=[];
    infer_info.relation_weights=[];
    infer_info.relation_map=work_info_step1.relation_map; 
    infer_info.single_weights=[];
    
    infer_info.e_num=train_info.e_num;
    
    infer_info.infer_cache=gen_infer_cache(train_info, infer_info);
   
    
    if train_info.do_infer_spectral
        infer_info.relation_map=double(infer_info.relation_map);
    end

end



function infer_cache=gen_infer_cache(train_info, infer_info)

infer_cache=[];

end

